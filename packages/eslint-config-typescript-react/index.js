module.exports = {
    "extends": [
        '@central-tech/eslint-config-react',
        '@central-tech/eslint-config-typescript/lib/shared.js'
    ],
    settings: {
        // Append 'ts' and 'tsx' extensions to Airbnb 'import/resolver' setting
        'import/resolver': {
            node: {
                extensions: ['.js', '.ts', '.jsx', '.tsx', '.json'],
            },
        },
        // Append 'ts' and 'tsx' extensions to Airbnb 'import/extensions' setting
        'import/extensions': ['.js', '.ts', '.mjs', '.jsx', '.tsx'],
    },
    rules: {
        // Append 'tsx' to Airbnb 'react/jsx-filename-extension' rule
        // https://github.com/yannickcr/eslint-plugin-react/blob/master/docs/rules/jsx-filename-extension.md
        'react/jsx-filename-extension': ['error', { extensions: ['.jsx', '.tsx'] }],
    },
};
