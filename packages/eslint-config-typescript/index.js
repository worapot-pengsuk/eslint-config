module.exports = {
    "extends": [
        '@central-tech/eslint-config',
        './lib/shared.js'
    ].map(require.resolve),
};